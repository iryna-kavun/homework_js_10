let eyeIcon = document.querySelectorAll('.eye-icon');
let passInput = document.querySelectorAll('.password-input');
let confirmButton = document.querySelector('.btn');

eyeIcon.forEach((el, i) => {

    el.addEventListener('click', () =>{
        if (el.classList.contains('fa-eye')) {
            passInput[i].setAttribute('type', 'text');
            el.classList.replace('fa-eye', 'fa-eye-slash');
        } else {
            passInput[i].setAttribute('type', 'password');
            el.classList.replace('fa-eye-slash', 'fa-eye');
        }
    })
});

confirmButton.addEventListener('click', () => {
   const valInitialInput = document.querySelector('#initial-input').value,
       valConfirmInput = document.querySelector('#confirm-input').value;
    if  (valInitialInput === valConfirmInput) {
        alert('You are welcome!');
    } else {
        const errorMessage = document.querySelector('#error-message');
        errorMessage.innerText = 'Values should be equal!!!';
        errorMessage.style.color = 'red';
    }
});







